# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from djangoplus.admin.views import index
from abstract import views

urlpatterns = [
    url(r'^$', views.home),
    url(r'^admin/$', index),
    url(r'^por-que-nao-traduzir-meu-resumo-do-tcc-com-o-google-tradutor/$', views.home),
    url(r'', include('djangoplus.admin.urls')),
]

