# -*- coding: utf-8 -*-
from datetime import datetime
from django.http.response import HttpResponse
from djangoplus.ui.components.navigation.breadcrumbs import httprr
from djangoplus.ui.components.paginator import Paginator
from abstract.forms import *
from djangoplus.decorators.views import view, action, dashboard
from djangoplus.mail import send_mail


@view('Formulário', login_required=False)
def home(request):
    form = SolicitacaoForm(request)
    if form.is_valid():
        form.save()
        send_mail('Tradução de Resumo - %s' % form.instance.pk, 'http://meuabstract.com.br/view/abstract/solicitacao/%s/ \n\n %s'%(form.instance.pk, form.instance.resumo) , 'brenokcc@yahoo.com.br')
        return httprr(request, '/abstract/formulario/', 'Solicitação enviada com sucesso.')
    return locals()


@view('Enviar', login_required=False)
def enviar(request):
    solicitacao = Solicitacao()
    solicitacao.nome = request.POST.get('name')
    solicitacao.email = request.POST.get('mail')
    solicitacao.resumo = request.POST.get('text')
    solicitacao.save()
    send_mail('Novo Resumo', '%s' % solicitacao.resumo, 'brenokcc@yahoo.com.br', actions=[('Visualizar', '/view/abstract/solicitacao/%s/'%solicitacao.pk)])
    return HttpResponse()


@action(Solicitacao, 'Traduzir', category=None, condition='pode_ser_traduzido')
def traduzir(request, pk):
    obj = Solicitacao.objects.get(pk=pk)
    title = 'Resumo de %s' % obj.nome
    if request.POST:
        obj.traducao = request.POST['traducao'].strip()
        obj.save()
        return httprr(request, '/view/abstract/solicitacao/{}/'.format(pk), 'Tradução salva com sucesso.')
    return locals()


@dashboard()
def ultimas_solicitacoes(request):
    widget = Paginator(request, Solicitacao.objects.all().order_by('-id'), 'Últimas Solicitações', readonly=True)
    return locals()

