# -*- coding: utf-8 -*-

from djangoplus.ui.components import forms

from abstract.models import Solicitacao


class SolicitacaoForm(forms.ModelForm):
    class Meta:
        model = Solicitacao
        fields = ['nome', 'email', 'resumo', 'arquivo']




