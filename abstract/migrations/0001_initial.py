# -*- coding: utf-8 -*-


from django.db import migrations, models
import datetime
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Solicitacao',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', djangoplus.db.models.fields.DateTimeField(default=datetime.datetime.now, verbose_name='Data')),
                ('nome', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='Nome')),
                ('email', djangoplus.db.models.fields.CharField(max_length=255, verbose_name='E-mail')),
                ('resumo', djangoplus.db.models.fields.TextField(default=b'', verbose_name='Resumo', blank=True)),
                ('arquivo', djangoplus.db.models.fields.FileField(upload_to=b'', null=True, verbose_name='Arquivo', blank=True)),
                ('traducao', djangoplus.db.models.fields.TextField(default=b'', verbose_name='Tradu\xe7\xe3o', blank=True)),
                ('data_envio', djangoplus.db.models.fields.DateTimeField(null=True, verbose_name='Dada do Envio')),
            ],
            options={
                'verbose_name': 'Solicita\xe7\xe3o de Tradu\xe7\xe3o',
                'verbose_name_plural': 'Solicita\xe7\xf5es de Tradu\xe7\xe3o',
            },
        ),
    ]
