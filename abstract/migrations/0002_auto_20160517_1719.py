# -*- coding: utf-8 -*-

# Generated by Django 1.9.6 on 2016-05-17 17:19


from django.db import migrations
import djangoplus.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('abstract', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='solicitacao',
            name='resposta',
            field=djangoplus.db.models.fields.TextField(blank=True, null=True, verbose_name='Resposta Inicial'),
        ),
        migrations.AddField(
            model_name='solicitacao',
            name='resposta_final',
            field=djangoplus.db.models.fields.TextField(blank=True, null=True, verbose_name='Resposta Final'),
        ),
    ]
