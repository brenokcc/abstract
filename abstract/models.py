# -*- coding: utf-8 -*-
from djangoplus.mail import send_mail
from djangoplus.db import models
from djangoplus.decorators import action, subset, meta
from datetime import datetime, date, timedelta
from decimal import Decimal


class SolicitacaoManager(models.DefaultManager):

    @subset('Pendentes', can_alert=True)
    def pendentes(self):
        return self.filter(data_envio__isnull=True)

    @meta('Total Anual', dashboard='top', formatter='line_chart')
    def total_anual(self):
        return self.filter(data__year=datetime.today().year, resposta__isnull=False).count('data')


class Solicitacao(models.Model):
    data = models.DateTimeField(verbose_name='Data', default=datetime.now, exclude=True)
    nome = models.CharField(verbose_name='Nome', search=True)
    email = models.CharField(verbose_name='E-mail', search=True)
    resumo = models.TextField(verbose_name='Resumo', blank=True, default='')
    arquivo = models.FileField(verbose_name='Arquivo', blank=True, null=True)

    valor = models.DecimalField('Valor (R$)', null=True, exclude=True)
    data_prevista_entrega = models.DateField(verbose_name='Dada Prevista para Entrega', null=True, exclude=True)
    resposta = models.TextField(verbose_name='Resposta Inicial', blank=True, null=True, exclude=True)

    traducao = models.TextField(verbose_name='Tradução', blank=True, default='', exclude=True)
    resposta_final = models.TextField(verbose_name='Resposta Final', blank=True, null=True, exclude=True)
    data_envio = models.DateTimeField(verbose_name='Dada do Envio', null=True, exclude=True)

    class Meta:
        verbose_name = 'Solicitação de Tradução'
        verbose_name_plural = 'Solicitações de Tradução'
        icon = 'fa-file-text'
        list_display = 'nome', 'email', 'data', 'data_envio'
        list_shortcut = True
        order_by = '-id'
        pdf = True

    fieldsets = (
        ('Dados Gerais', {'fields': (('data', 'nome'), ('email', 'arquivo'), 'resumo')}),
        ('Contato Inicial', {'fields': (('valor', 'data_prevista_entrega'), 'resposta')}),
        ('Tradução', {'fields': ('traducao',), 'actions':('traduzir',)}),
        ('Contato Final', {'fields': ('data_envio', 'resposta_final'), 'actions':('enviar_traducao', 'enviar_cobranca')}),
    )

    def __str__(self):
        return 'Solicitação %s' % self.pk

    def confirmar_recebimento_initial(self):
        return dict(data_prevista_entrega=date.today(), valor=37)

    @action('Confirmar Recebimento', category=None, condition='not resposta')
    def confirmar_recebimento(self, data_prevista_entrega, valor):
        return self.registrar_recebimento(data_prevista_entrega, valor, True)

    @action('Simular Recebimento', category='Simular', condition='not resposta')
    def simular_recebimento(self, data_prevista_entrega, valor):
        return self.registrar_recebimento(data_prevista_entrega, valor, False)

    def registrar_recebimento(self, data_prevista_entrega, valor, enviar_email):
        self.data_prevista_entrega = data_prevista_entrega
        self.valor = valor
        dias = ['segunda', 'terça', 'quarta', 'quinta', 'sexta', 'sábado', 'domingo']
        if valor == Decimal('37.00'):
            valor_str = 'O valor da tradução é R$ 37,00.'
        else:
            valor_str = 'Em virtude do tamanho, o valor da tradução ficou em R$ %s.' % str(valor).replace('.', ',')
        if data_prevista_entrega == date.today():
            prazo_str = 'O prazo de entrega é hoje (%s) até o final da tarde.' % dias[data_prevista_entrega.weekday()]
        elif data_prevista_entrega == (date.today() + timedelta(days=1)):
            prazo_str = 'O prazo de entrega é amanhã (%s) até o final da tarde.' % dias[data_prevista_entrega.weekday()]
        else:
            prazo_str = 'O prazo de entrega é %s (%s) até o final da tarde.' % (dias[data_prevista_entrega.weekday()], data_prevista_entrega.day)

        resposta = '''Olá %s,

Seu resumo foi recebido com sucesso.
%s %s
O pagamento só necessita ser realizado após o envio da tradução.
Confirma o interesse? Em caso afirmativo, qual meio você prefere? Transferência bancária (Banco do Brasil), boleto ou cartão?

Att,

Carlos Breno

        '''% (self.nome, valor_str, prazo_str)
        self.resposta = resposta
        if enviar_email:
            send_mail('Meu Abstract', resposta, self.email, reply_to='brenokcc@yahoo.com.br')
        self.save()

    def pode_ser_traduzido(self):
        return self.resposta and not self.resposta_final

    @action('Enviar Cobrança PagSeguro/UOL', category=None, condition='data_envio')
    def enviar_cobranca(self):
        import mechanicalsoup
        br = mechanicalsoup.StatefulBrowser()
        br.open('https://pagseguro.uol.com.br/')
        br.select_form()
        br['user'] = 'brenokcc@yahoo.com.br'
        br['pass'] = '128044cc'
        br.submit_selected()
        br.get_url()
        br.open('https://pagseguro.uol.com.br/operations/charging.jhtml')
        br.select_form()
        br['entries[0].email'] = self.email
        br['entries[0].name'] = self.nome
        br['items[0].description'] = 'Resumo'
        br['items[0].value'] = str(self.valor).replace('.', ',')
        br['items[0].quantity'] = '1'
        f = br.get_current_form()
        f.form.attrs.update(action='https://pagseguro.uol.com.br/operations/charging/create.jhtml')
        br.submit_selected()

    @action('Enviar Tradução', category=None, condition='pode_ser_enviada')
    def enviar_traducao(self, resposta_final):
        self.resposta_final = resposta_final
        self.data_envio = datetime.now()
        self.save()
        send_mail('Meu Abstract', resposta_final, self.email, reply_to='brenokcc@yahoo.com.br')

    def enviar_traducao_initial(self):
        return dict(resposta_final = '''Olá %s,

Segue a tradução do seu resumo dentro do prazo combinado.

Caso tenha optado por boleto ou cartão, você receberá na sequência um e-mail da PagSeguro/UOL contendo instruções para realização do pagamento.
Caso contrário, as informações bancárias são:

Banco do Brasil
Ag. 3293-X
Cc. 23384-6
Carlos Breno Pereira Silva

Parabéns pela conclusão do trabalho e boa sorte na apresentação.

Att,

Carlos Breno

=====

%s

=====
        '''% (self.nome, self.traducao))

    def pode_ser_enviada(self):
        return self.traducao and not self.data_envio


