# -*- coding: utf-8 -*-

from django.conf import settings
from djangoplus.test import TestCase
from djangoplus.admin.models import User
from djangoplus.test.decorators import testcase


class AppTestCase(TestCase):

    def test(self):
        User.objects.create_superuser('000.000.000-00', None, settings.DEFAULT_PASSWORD)
        self.execute_flow()

    @testcase('Cadastrar Solicitação', username='000.000.000-00')
    def cadastrar_solicitacao(self):
        self.click_button('Solicitações de Tradução')
        self.click_button('Cadastrar')
        self.enter('Nome', 'Carlos Breno')
        self.enter('E-mail', 'brenokcc@yahoo.com.br')
        self.enter('Resumo', 'Isso é um teste.')
        self.click_button('Salvar')
        self.wait()

    @testcase('Confirmar Recebimento', username='000.000.000-00')
    def confirmar_recebimento(self):
        self.click_button('Solicitações de Tradução')
        self.click_icon('Visualizar')
        self.click_button('Confirmar Recebimento')
        self.look_at_popup_window()
        self.enter('Valor (R$)', '37,00')
        self.enter('Dada Prevista para Entrega', '01/01/2017')
        self.click_button('Confirmar Recebimento')
        self.wait(6)

    @testcase('Traduzir', username='000.000.000-00')
    def traduzir(self):
        self.click_button('Solicitações de Tradução')
        self.click_icon('Visualizar')
        self.click_button('Traduzir')
        self.wait(3)
        self.enter('traducao', 'This is a test.')
        self.click_button('Salvar')
        self.wait()


    @testcase('Enviar Tradução', username='000.000.000-00')
    def enviar_traducao(self):
        self.click_button('Solicitações de Tradução')
        self.click_icon('Visualizar')
        self.click_button('Enviar Tradução')
        self.look_at_popup_window()
        self.enter('Resposta Final', 'Parabéns pelo trabalho')
        self.click_button('Enviar Tradução')
        self.wait()
